package cr.ac.ucr.cql.mipatrones.Models;

import java.util.List;

public interface ItemsRepository {
    //List<String> obtainItems() throws CantRetrieveItemsException;
    List<Persona> obtainItems() throws CantRetrieveItemsException;
}