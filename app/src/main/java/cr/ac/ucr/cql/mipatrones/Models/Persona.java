package cr.ac.ucr.cql.mipatrones.Models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Persona implements Parcelable {

    @PrimaryKey
    @NonNull
    private String id; // Id de la persona

    @ColumnInfo(name="nombre")
    private String nombre;

    @ColumnInfo(name="Hobbie")
    private String hobbie;

    @ColumnInfo(name="idImagen")
    private int idImagen;

    public static final String key = "Persona";

    public Persona(){};

    public Persona (String id, String nombre, String hobbie, int idImagen){
        this.id = id;
        this.nombre = nombre;
        this.hobbie = hobbie;
        this.idImagen = idImagen;
    }

    public Persona(Parcel in) { }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(nombre);
        dest.writeString(hobbie);
        dest.writeInt(idImagen);
    }

    public static final Creator<Persona> CREATOR = new Creator<Persona>()
    {
        @Override
        public Persona createFromParcel(Parcel in) {
            return new Persona(in);
        }
        @Override
        public Persona[] newArray(int size) {
            return new Persona[size];
        }
    };

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getHobbie() {
        return hobbie;
    }

    public void setHobbie(String hobbie) {
        this.hobbie = hobbie;
    }

    public int getIdImagen() {
        return idImagen;
    }

    public void setIdImagen(int idImagen) {
        this.idImagen = idImagen;
    }
}
