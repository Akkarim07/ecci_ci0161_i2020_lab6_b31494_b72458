package cr.ac.ucr.cql.mipatrones.Models;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface PersonaDAO
{
    @Query("SELECT * FROM persona")
    List<Persona> getAll();

    @Insert
    void insertAll(Persona... personas);

    @Query("DELETE FROM persona")
    void deleteAll();
}
