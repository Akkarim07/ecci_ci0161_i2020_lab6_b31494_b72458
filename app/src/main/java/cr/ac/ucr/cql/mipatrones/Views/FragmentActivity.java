package cr.ac.ucr.cql.mipatrones.Views;

import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import android.app.Fragment;

import cr.ac.ucr.cql.mipatrones.Models.Persona;
import cr.ac.ucr.cql.mipatrones.Presenters.FragmentActivityPresenter;
import cr.ac.ucr.cql.mipatrones.Presenters.FragmentActivityPresenterImpl;
import cr.ac.ucr.cql.mipatrones.R;

public class FragmentActivity extends Fragment implements FragmentActivityView{
    private Persona persona;
    private FragmentActivityPresenter mFragmentActivityPresenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstance) {
        Bundle bundle = getArguments();
        if(bundle != null){
            this.persona = bundle.getParcelable("persona");
        }
        mFragmentActivityPresenter = new FragmentActivityPresenterImpl(this);
        return inflater.inflate(R.layout.fragment_detalles, parent, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstance){
        super.onViewCreated(view, savedInstance);
        mFragmentActivityPresenter.onViewCreated(view, this.persona);
        Button closeButton = view.findViewById(R.id.close_btn);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity().getFragmentManager();
                for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                    fm.popBackStack();
                }
            }
        });
    }


    @Override
    public void setItem(View view, Persona persona) {
        TextView id = view.findViewById(R.id.fragment_id);
        TextView name = view.findViewById(R.id.fragment_name);
        TextView hobbie = view.findViewById(R.id.fragment_hobbie);
        ImageView image = view.findViewById(R.id.fragment_imagen);

        id.setText(persona.getId());
        name.setText(persona.getNombre());
        hobbie.setText(persona.getHobbie());
        image.setImageResource(persona.getIdImagen());
    }

}
