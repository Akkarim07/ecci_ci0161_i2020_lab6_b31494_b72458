package cr.ac.ucr.cql.mipatrones.Views;
import androidx.appcompat.app.AppCompatActivity;
import android.app.Fragment;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import cr.ac.ucr.cql.mipatrones.Models.Persona;
import cr.ac.ucr.cql.mipatrones.Presenters.MainActivityPresenter;
import cr.ac.ucr.cql.mipatrones.Presenters.MainActivityPresenterImpl;
import cr.ac.ucr.cql.mipatrones.R;

public class MainActivity extends AppCompatActivity implements
        // interface View (V) para implementar los metodos de UI
        MainActivityView,
        // interface para implementar el listener del metodo onItemClick de la lista
        AdapterView.OnItemClickListener
{
    private ListView mListView;
    private ProgressBar mProgressBar;
    private MainActivityPresenter mMainActivityPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mListView = findViewById(R.id.list);
        mListView.setOnItemClickListener(this);
        mProgressBar = findViewById(R.id.progress);
        // Llamada al Presenter
        mMainActivityPresenter = new MainActivityPresenterImpl(this);
    }
    @Override protected void onResume() {
        super.onResume();
        mMainActivityPresenter.onResume();
    }
    @Override public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    @Override public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override protected void onDestroy()
    {
        mMainActivityPresenter.onDestroy();
        super.onDestroy();
    }
    // Mostrar el progreso en la UI del avance de la tarea a realizar
    @Override public void showProgress()
    {
        mProgressBar.setVisibility(View.VISIBLE);
        mListView.setVisibility(View.INVISIBLE);
    }
    // Esconder el indicador de progreso de la UI
    @Override public void hideProgress() {
        mProgressBar.setVisibility(View.INVISIBLE);
        mListView.setVisibility(View.VISIBLE);
    }
    // Mostrar los items de la lista en la UI
    // Con la lista de items muestra la lista
    @Override public void setItems(List<Persona> items)
    {
        mListView.setAdapter(new CustomListAdapter(this, items));
    }
    // Mostrar mensaje en la UI
    @Override public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void startFragment(int position) {

        Persona selected = (Persona) mListView.getAdapter().getItem(position);
        if(selected != null) {
            Bundle bundle = new Bundle();
            Fragment fragment = new FragmentActivity();
            bundle.putParcelable("persona", selected);
            fragment.setArguments(bundle);
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.detalles_persona, fragment);
            ft.addToBackStack(null);
            ft.commit();
        }

    }

    // Evento al dar clic en la lista
    @Override public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        mMainActivityPresenter.onItemClicked(position);
    }

}