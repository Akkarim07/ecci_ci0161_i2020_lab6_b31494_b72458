package cr.ac.ucr.cql.mipatrones.Views;

import android.view.View;

import cr.ac.ucr.cql.mipatrones.Models.Persona;

public interface FragmentActivityView {
    void setItem(View view, Persona persona);
}
