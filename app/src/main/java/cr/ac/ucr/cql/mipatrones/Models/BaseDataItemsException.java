package cr.ac.ucr.cql.mipatrones.Models;

public class BaseDataItemsException extends Throwable {
    public BaseDataItemsException(String msg) {
        super(msg);
    }
}
