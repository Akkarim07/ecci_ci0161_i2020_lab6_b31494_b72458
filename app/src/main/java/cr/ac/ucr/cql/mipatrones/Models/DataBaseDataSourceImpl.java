package cr.ac.ucr.cql.mipatrones.Models;

import android.content.Context;

import java.util.List;
import androidx.room.Room;

import cr.ac.ucr.cql.mipatrones.R;


//Me parece que se siente que hacer async aquó

// Capa de datos (Model)
// Obtiene los valores de la fuente de datos
public class DataBaseDataSourceImpl implements DataBaseDataSource {
    private Context context;
    private ROOMDB db;

    public DataBaseDataSourceImpl(final Context context)
    {
        this.context = context;

        db = Room.databaseBuilder(context,  ROOMDB.class, "database-name").allowMainThreadQueries().fallbackToDestructiveMigration().build();
        db.personaDAO().deleteAll();
        db.personaDAO().insertAll(
                new Persona("0000", "Luis Carvajal", "Bailar", R.drawable.persona1),
                new Persona("0001", "Sebastián Cruz", "Programar", R.drawable.persona2),
                new Persona("0002", "Miguel Rojas", "Construir", R.drawable.persona3),
                new Persona("0003", "María Lang", "Ballet", R.drawable.persona4),
                new Persona("0004", "Cristina Orozco", "Flauta", R.drawable.persona5)
        );
    }


    @Override
    public List<Persona> obtainItems() throws BaseDataItemsException {
        List<Persona> items;
        try {
            // TODO: Obtener de la base de datos
            items = db.personaDAO().getAll();
        } catch (Exception e) {
            throw new BaseDataItemsException(e.getMessage());
        }
        return items;
    }
/*
    // para el ejemplo la inicializamos con datos dummy
    private List<String> createArrayList() {
        return Arrays.asList(
                "Soda 1",
                "Soda 2",
                "Soda 3",
                "Soda 4",
                "Soda 5",
                "Soda 6",
                "Soda 7",
                "Soda 8",
                "Soda 9",
                "Soda 10"
        );
    }*/
}