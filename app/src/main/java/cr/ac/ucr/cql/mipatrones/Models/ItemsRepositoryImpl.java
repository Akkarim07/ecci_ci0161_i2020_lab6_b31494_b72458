package cr.ac.ucr.cql.mipatrones.Models;
// Capa de datos (Model)
// El repositorio decide de que fuente de datos obtiene los valroes

import android.content.Context;

import java.util.List;


public class ItemsRepositoryImpl implements ItemsRepository
{
    private DataBaseDataSource mDataBaseDataSource;

    private Context context;

    public ItemsRepositoryImpl(Context context)
    {
        this.context = context;
    }

    @Override
    public List<Persona> obtainItems() throws CantRetrieveItemsException {
        List<Persona> items = null;
        try
        {
            mDataBaseDataSource = new DataBaseDataSourceImpl(context);
            items = mDataBaseDataSource.obtainItems();
        } catch (BaseDataItemsException e) {
            throw new CantRetrieveItemsException(e.getMessage());
        }
        return items;
    }
}



/*
public class ItemsRepositoryImpl implements ItemsRepository {
    private DataBaseDataSource mDataBaseDataSource;
    @Override
    public List<String> obtainItems() throws CantRetrieveItemsException {
        List<String> items = null;
        // Se realizan las llamadas a las fuentes de datos de donde se obtienen los datos
        // Ejemplo: base de datos local, archivos locales, archivos en la red, bases de
       // datos en la red
        try {
            mDataBaseDataSource = new DataBaseDataSourceImpl();
            items = mDataBaseDataSource.obtainItems();
        } catch (BaseDataItemsException e) {
            throw new CantRetrieveItemsException(e.getMessage());
        }
        return items;
    }
}
*/