package cr.ac.ucr.cql.mipatrones.Presenters;

import android.view.View;

import cr.ac.ucr.cql.mipatrones.Views.FragmentActivityView;
import cr.ac.ucr.cql.mipatrones.Models.Persona;

public class FragmentActivityPresenterImpl implements FragmentActivityPresenter{
    private FragmentActivityView mFragmentActivityView;

    public FragmentActivityPresenterImpl(FragmentActivityView view){
        mFragmentActivityView = view;
    }


    @Override
    public void onViewCreated(View view, Persona persona) {
        if(mFragmentActivityView != null){
            mFragmentActivityView.setItem(view, persona);
        }
    }
}
