package cr.ac.ucr.cql.mipatrones.Models;

import android.content.Context;

import java.util.List;

// Capa de Negocios (Presenter o Controller)
// Implementacion de GetListItemsInteractor de la capa de negocio (P o M) para obtener los resultados de la lista de elementos a mostrar
// Representa el Interactor (casos de uso), se comunica con las entidades y el presenter

public class GetListItemInteractorImpl implements GetListItemsInteractor {
    private ItemsRepository mItemsRepository;
    private Context context;

    //Fallaba si no pobía esto
    public GetListItemInteractorImpl(Context context)
    {
        this.context = context;
    }
    //Creo que sirve
    @Override public void getItems(final OnFinishedListener listener) {
        // Enviamos el hilo de ejecucion con un delay para mostar la barra de progreso
        List<Persona> items = null;
        mItemsRepository = new ItemsRepositoryImpl(context);
        try
        {
            items = mItemsRepository.obtainItems();
        }
        catch (CantRetrieveItemsException e)
        {
            e.printStackTrace();
        }
        listener.onFinished(items);
    }
}


