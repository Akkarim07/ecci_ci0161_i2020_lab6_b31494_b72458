package cr.ac.ucr.cql.mipatrones.Models;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Persona.class}, version = 2)
public abstract class ROOMDB extends RoomDatabase
{
    public abstract PersonaDAO personaDAO();
}