package cr.ac.ucr.cql.mipatrones.Presenters;

import android.view.View;

import cr.ac.ucr.cql.mipatrones.Models.Persona;

public interface FragmentActivityPresenter {

    void onViewCreated(View view, Persona persona);
}
