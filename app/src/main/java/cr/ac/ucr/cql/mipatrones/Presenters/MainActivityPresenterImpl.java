package cr.ac.ucr.cql.mipatrones.Presenters;
// Capa de presentacion (Presenter)
// Presenter (P) -> Implementacion de MainActivityPresenter y de GetListItemsInteractor.OnFinishedListener

import android.content.Context;

import java.util.List;

import cr.ac.ucr.cql.mipatrones.Views.MainActivityView;
import cr.ac.ucr.cql.mipatrones.Models.GetListItemInteractorImpl;
import cr.ac.ucr.cql.mipatrones.Models.GetListItemsInteractor;
import cr.ac.ucr.cql.mipatrones.Models.Persona;

public class MainActivityPresenterImpl implements
        // Implementacion de MainActivityPresenter de la Capa (P)
        MainActivityPresenter,
        // La interface OnFinishedListener de GetListItemsInteractor para obtener el valor de retorno de la capa Iteractor (P)
        GetListItemsInteractor.OnFinishedListener
{
    private MainActivityView mMainActivityView;
    private GetListItemsInteractor mGetListItemsInteractor;

    public MainActivityPresenterImpl(MainActivityView mainActivityView)
    {
        this.mMainActivityView = mainActivityView;
        // Capa de negocios (Interactor)
        this.mGetListItemsInteractor = new GetListItemInteractorImpl((Context)mainActivityView);
    }
    @Override public void onResume()
    {
        if (mMainActivityView != null) {
            mMainActivityView.showProgress();
        }
        // Obtener los items de la capa de negocios (Interactor)
        mGetListItemsInteractor.getItems(this);
    }

    // Evento de clic en la lista
    @Override public void onItemClicked(int position)
    {
        if(mMainActivityView != null){
            mMainActivityView.startFragment(position);
        }
    }

    @Override public void onDestroy()
    {
        mMainActivityView = null;
    }

    @Override public void onFinished(List<Persona> items)
    {
        if (mMainActivityView != null)
        {
            mMainActivityView.setItems(items);
            mMainActivityView.hideProgress();
        }
    }

    // Retornar la vista
    public MainActivityView getMainView()
    {
        return mMainActivityView;
    }
}