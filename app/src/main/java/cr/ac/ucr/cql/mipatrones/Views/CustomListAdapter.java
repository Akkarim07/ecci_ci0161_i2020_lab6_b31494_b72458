package cr.ac.ucr.cql.mipatrones.Views;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;

import cr.ac.ucr.cql.mipatrones.Models.Persona;
import cr.ac.ucr.cql.mipatrones.R;

public class CustomListAdapter extends ArrayAdapter<Persona> {
    private final Activity context;
    private final List<Persona> personas;
    public CustomListAdapter(Activity context, List<Persona> personas){
        super(context, R.layout.custom_list, personas);
        this.context = context;
        this.personas = personas;
    }

    public View getView(int position, View view, ViewGroup parent){
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.custom_list, null, true);

        TextView idNombre = rowView.findViewById(R.id.id_nombre);
        ImageView imagen = rowView.findViewById(R.id.imagen);
        TextView hobbie = rowView.findViewById(R.id.hobbie);

        idNombre.setText(personas.get(position).getId() + " : " + personas.get(position).getNombre());
        imagen.setImageResource(personas.get(position).getIdImagen());
        hobbie.setText(personas.get(position).getHobbie());

        return rowView;
    }
}
