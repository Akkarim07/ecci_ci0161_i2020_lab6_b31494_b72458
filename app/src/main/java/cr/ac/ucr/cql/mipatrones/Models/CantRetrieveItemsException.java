package cr.ac.ucr.cql.mipatrones.Models;

public class CantRetrieveItemsException extends Throwable {
    public CantRetrieveItemsException(String msg) {
        super(msg);
    }
}
